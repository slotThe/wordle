# wordle

A naïve wordle implementation, for Benjamin.

## Usage

Build and start with

``` console
$ lein uberjar && java -jar wordle-0.1.0-standalone.jar
```

Letters are entered in the following way:

    c:*letter*:*number*   *letter* is in word, but _not_ at position *number*,
    cp:*letter*:*number*  *letter* is in word, at position *number*,
    n:*letter*            *letter* is not in word.

Note that we don't do the optimisation "once a *letter* is given,
entering n:*letter* means that this letter occurs nowhere else in the
word", so don't enter `c:a:1 n:a` or similar.
