(ns wordle.core
  (:require [clojure.string :as str]
            [clojure.set    :as set])
  (:gen-class))

(defn- parse []
  (str/split-lines (slurp "./resources/words.txt")))

(defn- get-ranking
  "For a given list of words, compute an ordered list of characters of
  relative frequencies.  I.e., the first character in the list occurs in
  the most words in WORD-LIST (and so on)."
  [word-list]
  (let [n (count word-list)]
    (->> word-list
         (map #(zipmap (set %) (repeat 1)))       ; [ { [\a 1] [\f 1] ... } ... ]
         (apply merge-with +)                     ; { [\a 20]   [\b 100]  ... }
         (into [] (map (fn [[k v]] [k (- n v)]))) ; [ [\a 1980] [\b 1900] ... ]
         (sort-by second compare)                 ; [ [\b 1900] [\a 1980] ... ]
         (map first))))                           ; [ \b \a ... ]

(defn- guess-word
  "Guess a word.  Try all of the words with no repetition of characters
  first, as these tend to give off more information.  If no word
  matches, fall back to all words instead.

  Returns `nil` if no match could be found."
  [word-list]
  (let [ranking (get-ranking word-list)
        no-rep (filter (partial apply distinct?) word-list)]
    (letfn [(make-guess [wl]
              (loop [n 5]
                (let [avail-chars (set (take n ranking))
                      ;; Guessed words may only use the given characters.
                      guesses (filter #(set/subset? (set %) avail-chars) wl)]
                  (if (seq guesses)     ; `nil` when `guess` is empty
                    (first guesses)
                    (recur (inc n))))))]
      (if-let [guess (make-guess no-rep)]
        guess
        (seq (make-guess word-list))))))

;; A letter with a given refinement.
(defrecord Letter [state letter])

(defn- parse-refinement
  "Parse a user-given refinement.  The syntax is as follows:

      c:*letter*:*number*   *letter* is in word, but _not_ at position *number*,
      cp:*letter*:*number*  *letter* is in word, at position *number*,
      n:*letter*            *letter* is not in word.

  Note that we don't do the optimisation \"once a *letter* is given,
  entering n:*letter* means that this letter occurs nowhere else in the
  word\"."
  [line]
  (->> line                             ; cp:r:1    c:a    n:e
       (#(str/split % #" "))            ; [cp:r:1   c:a    n:e]
       (map #(str/split % #":"))        ; [[cp r 1] [c a] [n e]]
       (keep (fn [[k & v]]              ; [[:state :correct, :letter [r 1]] ...]
               (let [l (ffirst v)]
                 (case k
                   "c" (->Letter :correct [l (read-string (second v))])
                   "cp" (->Letter :correct-pos [l (read-string (second v))])
                   "n" (->Letter :not-in l)
                   nil))))))

(defn- prune-word-list
  "Prune the words list based on the given refinements as described in
  `parse-refinement`."
  [word-list refinements]
  (let [{:keys [correct correct-pos not-in]} (group-by :state refinements)]
    (letfn [(at [ix l] (nth l (dec ix)))
            (correct-f [wl]
              (reduce (fn [lst [l p]]
                        (filterv #(and (some #{l} %)
                                       (not= l (at p %)))
                                 lst))
                      wl
                      (mapv :letter correct)))
            (correct-pos-f [wl]
              (reduce (fn [lst [l p]] (filterv #(= l (at p %)) lst))
                      wl
                      (mapv :letter correct-pos)))
            (not-in-f [wl]
              (if not-in
                (filter #(not-any? (set (mapv :letter not-in)) %) wl)
                wl))]
      (correct-pos-f (correct-f (not-in-f word-list))))))

(defn -main
  "Play some wordle."
  [& _args]
  (let [word-list (parse)]
    (println "Guess word: " (guess-word word-list))
    (loop [wl word-list]
      (let [refine-input (parse-refinement (read-line))
            pruned-list (prune-word-list wl refine-input)]
        (if (= 1 (count pruned-list))
          (println "Winner: " (first pruned-list))
          (do (println "\nRemaining words: " pruned-list)
              (println "Guess word: " (guess-word pruned-list))
              (recur pruned-list)))))))
